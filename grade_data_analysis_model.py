import json
import sys
from utils.csv2dict import csv2dict
from utils.normalized_name import normalized_name
from sklearn.feature_extraction import DictVectorizer
from sklearn.linear_model import LogisticRegression

training_items = ['fico_score_weighted', 'term_months', 'intelliscore', 'dscr', 'bdfs_score', 'annual_revenue', 'business_founding_date', 'approved_amount', 'vantage_score_3_weighted', 'business_founding_date']

target_item = ['credit_grade']

class CreditGrade: 
    def __init__(self, file_path = None):
        self.vectorizer = DictVectorizer(sparse = False)
        self.classifier = LogisticRegression(C=0.01, penalty='l1')
        self.company_info = csv2dict(file_path).get_dict_from_csv()

    @staticmethod
    def parse_input(data, low_accuracy_mode = False):
        data_set = []
        target_set = []
        for item in data:
            x = {}
            for key in item.keys():
                if key in training_items:
                    x[key] = item[key]
                elif key in target_item:
                    if low_accuracy_mode:
                        item[key] = item[key][0]
                    target_set.append(item[key])
            data_set.append(x)
        return data_set, target_set
    
    def get_company_info(self, company_name):
        normalized_company_name = normalized_name(company_name)
        for item in self.company_info:
            if normalized_name(item.get('company_name', '')) == normalized_company_name:
                return item
    
    def insert_company_info(self, company_info):
        self.company_info.append(company_info)

    def train(self):
        data_set, target_set = self.parse_input(self.company_info)
        vectorized_data_set = self.vectorizer.fit_transform(data_set)
        self.classifier.fit(vectorized_data_set, target_set)

    def predict(self, data):
        data_set, _ = self.parse_input(data)
        vectorized_data_set = self.vectorizer.transform(data_set) 
        return self.classifier.predict(vectorized_data_set)


grade_data_analysis_model = CreditGrade('sample_data.csv')
grade_data_analysis_model.train()
