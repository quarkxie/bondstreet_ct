import re
import csv
import pandas as pd

pattern = {'date' : '(((19|20)([2468][048]|[13579][26]|0[48])|2000)[/-]02[/-]29|((19|20)[0-9]{2}[/-](0[4678]|1[02])[/-](0[1-9]|[12][0-9]|30)|(19|20)[0-9]{2}[/-](0[1359]|11)[/-](0[1-9]|[12][0-9]|3[01])|(19|20)[0-9]{2}[/-]02[/-](0[1-9]|1[0-9]|2[0-8])))'}
date_pattern = re.compile(pattern['date'])

class csv2dict:

    class __csv2dict:
        def __init__(self, file_path = None):
            self.file_path = file_path
    
        @staticmethod
        def convert_to_py(value):
            try:
                return eval(value)
            except:
                if date_pattern.match(value):
                    diff = pd.datetime.now() - pd.to_datetime(value)
                    return diff.days
                else:
                    return str(value)

        def get_dict_from_csv(self):
            if self.file_path:
                json_dict = []
                with open(self.file_path) as csv_file:
                    reader = csv.DictReader(csv_file)
                    json_dict = []
                    for row in reader:
                        for key, value in row.iteritems():
                            row[key] = self.convert_to_py(value) or 0
                        json_dict.append(row)
                return json_dict
                
        def parse_data(self, item):
            for key, value in item.iteritems():
                item[key] = self.convert_to_py(value) or 0
            return item 

    instance = None
    def __init__(self, file_path = None):
        if not csv2dict.instance:
            csv2dict.instance = csv2dict.__csv2dict(file_path)
        elif file_path:
            csv2dict.instance.file_path = file_path

    def __getattr__(self, name):
        return getattr(self.instance, name)
            
