from bottle import route, run, request, abort
from grade_data_analysis_model import grade_data_analysis_model
from utils.csv2dict import csv2dict

VERSION = '0.1'
prefix = 'api/v{0}'.format(VERSION)

@route('/{0}/credit_grade/<company_name>'.format(prefix), method='GET')
def get_credit_grade(company_name):
    company_info = grade_data_analysis_model.get_company_info(company_name)
    if not company_info:
    	abort(400, 'company name {0} is not found in our database'.format(company_name))

    return {'company name' : company_info['company_name'], 'credit grade' : company_info['credit_grade']}

@route('/{0}/credit_grade/add_new_company'.format(prefix), method='POST')
def add_company_info():
    try:
        input_data = csv2dict().parse_data(request.json)
    except ValueError as e:
        abort(400, e)

    input_data.update({'credit_grade' : grade_data_analysis_model.predict([input_data])[0]})
    grade_data_analysis_model.insert_company_info(input_data)


run(host='localhost', port=8080, debug=True)
