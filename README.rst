Bond Street Credit Grade
========================

This is a project is a simplified model to evaluate a company's credit grade. The model is created with a simple logistic regression learning algorithm with python's sklearn library. The vectorizor will convert a dictionary containing a company's info into a matrix used for the learning algorithm. There is also a sample implementation of python web API used to access the model (created with python bottle library). 

============
Installation
============

Three libraries are required installed to run this project. 

For python pandas please see `install pandas <http://pandas.pydata.org/pandas-docs/stable/install.html>`_.

For python sklearn please see `install scikit-learn <http://scikit-learn.org/stable/install.html>`_.

`pip install bottle` to install python bottle web framework.

=============
API Resources 
=============

### GET /credit_grade/<company_name>

Example: `http://example.com/api/v0.1/credit_grade/bond_street`

Response body:
    
    {
        "company name" : "bond street",
        "credit grade" : "A1",
        "timestamp" : "2012-12-15 10:14:51.898000"
    }

### GET /credit_grade/select/<filters:list>

Example: `http://example.com/api/v0.1/credit_grade/select/term_months|greater_than|36%dscr|less_than|1.5`

Will be implemented in next update.

### POST /credit_grade/add_new_company

Example: `http://example.com/api/v0.1/credit_grade/add_new_company`

Request body:

    {
        "company_name": "East Villege Whole Food",
        "business_category": "Convenience Stores",
        "fico_score_weighted": "755", 
        "intelliscore": "99", 
        "dscr": "1.2", 
        "loan_purpose": "outfit", 
        "approved_amount": "50000", 
        "connected_quickbooks": "0", 
        "legal_structure": "c_corp", 
        "organization_state": "AZ", 
        "connected_bank": "0", 
        "bdfs_score": "491", 
        "annual_revenue": "462725", 
        "business_founding_date": "1991-08-09", 
        "term_months": "36", 
        "vantage_score_3_weighted": "712"
    }

=====
Test
=====

Run the service under debug mode and run `test/*.sh` to test the service. 

=======
Authors
=======

Qiuchen Xie

==========
Contribute
==========



==========
Change Log
==========



=======
Licence
=======


