 #!/bin/bash

curl -i -H "Content-Type: application/json" -X POST -d '{"company_name": "East Villege Whole Food", "business_category": "Convenience Stores", "fico_score_weighted": "755", "intelliscore": "99", "dscr": "1.2", "loan_purpose": "outfit", "approved_amount": "50000", "connected_quickbooks": "0", "legal_structure": "c_corp", "organization_state": "AZ", "connected_bank": "0", "bdfs_score": "491", "annual_revenue": "462725", "business_founding_date": "1991-08-09", "term_months": "36", "vantage_score_3_weighted": "712"}' http://localhost:8080/api/v0.1/credit_grade/add_new_company
