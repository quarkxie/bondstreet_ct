def normalized_name(raw_name):
    return raw_name.replace('_', ' ').strip().lower()
